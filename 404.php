<!doctype html style="margin: 0 !important;">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, minimal-ui" />
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS" href="<?php bloginfo('rss2_url'); ?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/src/img/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
	<link rel="stylesheet" href="https://use.typekit.net/bxw0zrg.css">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div class="flex">
		<div class="small tacenter">
			<h2>Something went wrong...</h2>
			<a class="button" href="<?php echo site_url(); ?>">Go back</a>
		</div>
	</div>
</body>

</html>
