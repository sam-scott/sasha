<?php get_header(); ?>

<?php include('inc-edit.php');?>

<!-- banner -->

<?php get_template_part( 'template-parts/banner' ); ?>

<!-- categories -->

<?php get_template_part( 'template-parts/category-nav' ); ?>

<!-- projects -->

<section class="thick">
	<?php get_template_part( 'template-parts/projects' ); ?>
</section>

<!-- call to action -->

<?php get_template_part( 'template-parts/action' ); ?>


<?php get_footer(); ?>
