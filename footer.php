</div>

<footer class="xthin tdark">
	<div class="large flex">
		<div class="taleft">
			<a class="logo" href="<?php bloginfo('url'); ?>"><?php include(get_template_directory() . '/src/img/logo.svg'); ?></a>
		</div>

		<div class="taright">
			<p>Copyright &copy; <?php echo date('Y'); ?></p>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
