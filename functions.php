<?php

require_once('functions/theme.php');
require_once('functions/default.php');
require_once('functions/enqueue.php');
require_once('functions/acf.php');
require_once('functions/images.php');
require_once('functions/pagination.php');
require_once('functions/woocommerce.php');
require_once('functions/breadcrumbs.php');

?>
