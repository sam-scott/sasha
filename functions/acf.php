<?php

///// option page /////

if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'position' => '50',
    'page_title'=> 'Theme Options',
  ));
}

///// register custom posts /////

function custom_cpt() {
  $labels = array(
    'name'                   => _x( 'Collections', 'Post type general name', 'textdomain' ),
    'singular_name'          => _x( 'Collection', 'Post type singular name', 'textdomain' ),
    'menu_name'              => _x( 'Collections', 'Admin Menu text', 'textdomain' ),
    'name_admin_bar'         => _x( 'Collections', 'Add New on Toolbar', 'textdomain' ),
    'add_new'                => __( 'Add New', 'textdomain' ),
    'add_new_item'           => __( 'Add New Collection', 'textdomain' ),
    'new_item'               => __( 'New Collection', 'textdomain' ),
    'edit_item'              => __( 'Edit Collection', 'textdomain' ),
    'view_item'              => __( 'View Collection', 'textdomain' ),
    'all_items'              => __( 'All Collections', 'textdomain' ),
    'search_items'           => __( 'Search Collections', 'textdomain' ),
    'parent_item_colon'      => __( 'Parent Collections:', 'textdomain' ),
    'not_found'              => __( 'No collections found.', 'textdomain' ),
    'not_found_in_trash'     => __( 'No collections found in Trash.', 'textdomain' ),
    'archives'               => _x( 'Collection Archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
    'insert_into_item'       => _x( 'Insert into collection', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
    'uploaded_to_this_item'  => _x( 'Uploaded to this collection', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
    'filter_items_list'      => _x( 'Filter collections list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
    'items_list_navigation'  => _x( 'Collections list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
    'items_list'             => _x( 'Collections list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
  );

  $args = array(
    'labels'                => $labels,
    'public'                => true,
    'publicly_queryable'    => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'collections' ),
    'capability_type'       => 'post',
    'has_archive'           => true,
    'hierarchical'          => false,
    'menu_position'         => null,
    'supports'              => array( 'title', 'editor', 'thumbnail'),
    'menu_icon'             => 'dashicons-book'
  );

  register_post_type( 'collections' , $args );

  // $labels = array(
  //   'name'                  => _x( 'Projects', 'Post type general name', 'textdomain' ),
  //   'singular_name'         => _x( 'Project', 'Post type singular name', 'textdomain' ),
  //   'menu_name'             => _x( 'Projects', 'Admin Menu text', 'textdomain' ),
  //   'add_new'               => __( 'Add New', 'textdomain' ),
  //   'add_new_item'          => __( 'Add New Project', 'textdomain' ),
  //   'new_item'              => __( 'New Project', 'textdomain' ),
  //   'edit_item'             => __( 'Edit Project', 'textdomain' ),
  //   'view_item'             => __( 'View Project', 'textdomain' ),
  //   'all_items'             => __( 'All Projects', 'textdomain' ),
  //   'search_items'          => __( 'Search Projects', 'textdomain' ),
  //   'parent_item_colon'     => __( 'Parent Projects:', 'textdomain' ),
  // );
  //
  // $args = array(
  //   'labels'                => $labels,
  //   'hierarchical'          => true,
  //   'public'                => true,
  //   'show_ui'               => true,
  //   'show_in_menu'          => true,
  //   'show_in_nav_menus'     => true,
  //   'show_in_admin_bar'     => true,
  //   'can_export'            => true,
  //   'has_archive'           => true,
  //   'exclude_from_search'   => false,
  //   'publicly_queryable'    => true,
  //   'capability_type'       => 'page',
  //   'taxonomies'            => array('category'),
  //   'rewrite'               => array( 'slug' => 'projects' ),
  //   'menu_icon'             => 'dashicons-admin-customizer'
  // );
  //
  // register_post_type( 'projects' , $args );
}
add_action( 'init', 'custom_cpt' );

function custom_post_type(){
  $get_post_type = get_post_type_object('post');
  $labels = $get_post_type->labels;
  $labels->name = 'Projects';
  $labels->singular_name = 'Projects';
  $labels->all_items = 'All Projects';
  $labels->menu_name = 'Projects';
  $labels->name_admin_bar = 'Projects';
}
add_action( 'init', 'custom_post_type' );


// function custom_cpt_taxonomy() {
//   $labels = array(
// 		'name'                  => _x( 'Categories', 'taxonomy general name', 'textdomain' ),
// 		'singular_name'         => _x( 'Category', 'taxonomy singular name', 'textdomain' ),
//     'menu_name'             => __( 'Categories', 'textdomain' ),
//     'add_new_item'          => __( 'Add New Category', 'textdomain' ),
//     'new_item_name'         => __( 'New Category Name', 'textdomain' ),
//     'edit_item'             => __( 'Edit Category', 'textdomain' ),
//     'all_items'             => __( 'All Categories', 'textdomain' ),
// 		'search_items'          => __( 'Search Categories', 'textdomain' ),
// 		'popular_items'         => __( 'Popular Categories', 'textdomain' ),
// 		'update_item'           => __( 'Update Category', 'textdomain' ),
// 	);
//
// 	$args = array(
// 		'labels'                => $labels,
//     'public'                => true,
// 		'hierarchical'          => true,
// 		'show_ui'               => true,
// 		'show_in_menu'          => true,
// 		'show_in_nav_menus'     => true,
// 		'query_var'             => true,
// 		'show_admin_column'     => true,
// 		'show_in_rest'          => false,
// 		'show_in_quick_edit'    => true,
// 	);
//
// 	register_taxonomy( 'category', 'projects', $args );
// }
// add_action( 'init', 'custom_cpt_taxonomy' );

?>
