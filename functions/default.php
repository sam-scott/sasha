<?php

///// check no robots /////

function my_admin_notice(){
  if ( !get_option('blog_public') ){
    echo '<div class="error"><p>Search engines are blocked</p></div>';
  }
}
add_action( 'admin_notices', 'my_admin_notice' );



///// conditional ie /////

function add_ie_html5_shim () {
	global $is_IE;
	if ($is_IE){
 		echo '<!--[if lt IE 9]>';
  	echo '<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>';
  	echo '<![endif]-->';
    }
}
add_action('wp_head', 'add_ie_html5_shim');



///// no pingbacks /////

add_filter( 'xmlrpc_methods', function( $methods ) {
  unset( $methods['pingback.ping'] );
  return $methods;
} );



///// nice url /////

function niceurl($url) {
	$url = str_replace('http://', '', $url);
	$url = str_replace('https://', '', $url);
	$url = str_replace('www.', '', $url);
	$url = rtrim($url, "/");
    return $url;
}



///// get page url /////

function curPageURL() {
  $pageURL = 'http';
  if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}

  $pageURL .= "://";
  if ($_SERVER["SERVER_PORT"] != "80") {
    $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
  } else {
    $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
  }
  return $pageURL;
}



///// add documentation /////

function register_documentation(){
   add_menu_page(
       __( 'Documentation', 'textdomain' ),
       'Documentation',
       'manage_options',
       'custompage',
       'documentation',
       'dashicons-format-aside',
       3
   );
}
add_action( 'admin_menu', 'register_documentation' );

function documentation() {
   echo '<style media="screen">
       .docs {
           width: 90%;
           height: 100%;
           padding-bottom: 105%;
       }
       iframe {
           position: absolute;
           top:0;
           left: 0;
           width: 100%;
           height: 100%;
       }
   </style>
   <div class="docs">
       <iframe src="/docs/" width="100%" height="100%"></iframe>
   </div>';
}

?>
