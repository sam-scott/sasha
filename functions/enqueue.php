<?php

///// enqueue scripts and styles /////

function theme_enqueue(){

  // wp_enqueue_style( 'style-name', get_stylesheet_uri() );

  wp_enqueue_style( 'custom-style', get_template_directory_uri() . '/style.css', array(), null );

  wp_enqueue_style( 'override-style', get_template_directory_uri() . '/src/css/override.css', array(), null );

  wp_enqueue_script('jquery');
  wp_enqueue_script( 'main-js', get_template_directory_uri() . '/src/js/main.js', array(), '', true);
  wp_enqueue_script( 'flickity-js', get_template_directory_uri() . '/src/js/flickity.pkgd.min.js');
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue' );

?>
