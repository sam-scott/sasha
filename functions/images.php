<?php

///// image sizes /////

add_image_size( '100w', 100, 0, false );
add_image_size( '200w', 200, 0, false );
add_image_size( '400w', 400, 0, false );
add_image_size( '600w', 600, 0, false );
add_image_size( '800w', 800, 0, false );
add_image_size( '1200w', 1200, 0, false );
add_image_size( '1800w', 1800, 0, false );



///// image quality /////

function gpp_jpeg_quality_callback($arg) {
  return (int)75; // change 100 to whatever you prefer, but don't go below 60
}
add_filter('jpeg_quality', 'gpp_jpeg_quality_callback');



///// svg support /////

function add_file_types_to_uploads($file_types){
  $new_filetypes = array();
  $new_filetypes['svg'] = 'image/svg+xml';
  $file_types = array_merge($file_types, $new_filetypes );
  return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');

?>
