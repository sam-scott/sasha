<?php

///// login logo /////

function my_login_logo() { ?>
  <style type="text/css">
    .login h1 a {
      background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.svg);
      margin: 0 auto;
      height: 150px;
      width: 80%;
      background-size: contain;
    }
  </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
  return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
  return get_bloginfo( 'title' );
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );



///// remove admin toolbar /////

add_filter('show_admin_bar', '__return_false');



///// remove gutenburg /////

add_filter('use_block_editor_for_post', '__return_false', 10);
add_filter('use_block_editor_for_post_type', '__return_false', 10);



///// custom menu /////

function register_custom_menu() {
	register_nav_menu('main', 'Main Menu');
	register_nav_menu('foot', 'Footer Menu');
	register_nav_menu('serv', 'Services Menu');
}
add_action('init', 'register_custom_menu');



///// title tag support /////

function theme_slug_setup() {
  add_theme_support( 'title-tag' );
  add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'theme_slug_setup' );



///// tidy admin /////

function remove_menus_custom() {
  global $current_user;
  $username = $current_user->user_login;

  remove_menu_page( 'edit-comments.php' );
  remove_menu_page( 'tools.php' );

  // if ($username != 'admin@thrive') {
  //   remove_menu_page( 'edit-comments.php' );
  //   remove_menu_page( 'themes.php' );
  //   remove_menu_page( 'users.php' );
  //   remove_menu_page( 'plugins.php' );
  //   remove_menu_page( 'tools.php' );
  //   remove_menu_page( 'options-general.php' );
  //   remove_menu_page( 'edit.php?post_type=acf-field-group' );
  // }
}
add_action( 'admin_menu', 'remove_menus_custom' );



///// store locator //////

function custom_admin_marker_dir() {
  $admin_marker_dir = get_stylesheet_directory() . '/wpsl-markers/';
  return $admin_marker_dir;
}
add_filter( 'wpsl_admin_marker_dir', 'custom_admin_marker_dir' );

define( 'WPSL_MARKER_URI', dirname( get_bloginfo( 'stylesheet_url') ) . '/wpsl-markers/' );

function custom_js_settings( $settings ) {
  $settings['startMarker'] = '';
  return $settings;
}
add_filter( 'wpsl_js_settings', 'custom_js_settings' );



///// cpt categories //////

function query_post_type($query) {
  if((is_category() || is_tag()) && !is_admin()) {
    $query->set('projects', 'posts');
    return $query;
  }
}
add_filter('pre_get_posts', 'query_post_type');


?>
