<?php

///// clean single product /////

remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash' );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );



///// remove price decimals /////

add_filter( 'woocommerce_price_trim_zeros', '__return_true' );



///// remove additional info & order notes from checkout /////

// add_filter( 'woocommerce_enable_order_notes_field', '__return_false', 9999 );
//
// function remove_order_notes( $fields ) {
//   unset($fields['order']['order_comments']);
//   return $fields;
// }
// add_filter( 'woocommerce_checkout_fields' , 'remove_order_notes' );



///// remove woo messages /////

function remove_add_to_cart_message() {
  return;
}
add_filter( 'wc_add_to_cart_message', 'remove_add_to_cart_message' );



///// redirect to checkout on add to cart /////

function redirect_checkout_add_cart() {
  return wc_get_checkout_url();
}
add_filter( 'woocommerce_add_to_cart_redirect', 'redirect_checkout_add_cart' );



///// strip checkout form /////

function custom_override_checkout_fields_ek( $fields ) {
	unset($fields['billing']['billing_company']);
	unset($fields['billing']['billing_address_1']);
	unset($fields['billing']['billing_address_2']);
	unset($fields['billing']['billing_country']);
	unset($fields['billing']['billing_postcode']);
	unset($fields['billing']['billing_state']);
	unset($fields['billing']['billing_city']);

	return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields_ek', 99 );



///// fix checkout placeholders /////

function override_billing_checkout_fields( $fields ) {
  $fields['billing']['billing_first_name']['placeholder'] = 'First Name';
  $fields['billing']['billing_last_name']['placeholder'] = 'Last Name';
  $fields['billing']['billing_phone']['placeholder'] = 'Phone Number';
  $fields['billing']['billing_email']['placeholder'] = 'Email Address';
  $fields['order']['order_comments']['placeholder'] = 'Further information';
  return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'override_billing_checkout_fields', 20, 1 );



///// add message field to checkout //////

// function custom_override_checkout_fields( $fields ) {
//   $fields['billing']['message'] = array(
//     'type' => 'textarea',
//     'placeholder'   => _x('Your message', 'placeholder', 'woocommerce'),
//     'required'  => false,
//     'clear'     => true
//   );
//
//   return $fields;
// }
// // add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
//
//
// function custom_woocommerce_email_order_meta_fields( $fields, $sent_to_admin, $order ) {
//   $fields['billing']['message'] = array(
//       'label' => __( 'Message' ),
//       'value' => get_post_meta( $order->id, 'billing_message', true ),
//   );
//   return $fields;
// }
// // add_filter( 'woocommerce_email_order_meta_fields', 'custom_woocommerce_email_order_meta_fields', 10, 3 );



///// custom checkout button //////

function misha_custom_button_text( $button_text ) {
  return 'Enquire Now';
}
add_filter( 'woocommerce_order_button_text', 'misha_custom_button_text' );



function success_order_redirect( $order_id ){
  $order = wc_get_order( $order_id );
  $url = '/thank-you';
  if ( ! $order->has_status( 'failed' ) ) {
      wp_safe_redirect( $url );
      exit;
  }
}
add_action( 'woocommerce_thankyou', 'success_order_redirect');

?>
