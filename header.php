<!doctype html>
<html lang="en">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=5.0, minimal-ui" />
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS" href="<?php bloginfo('rss2_url'); ?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/src/img/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
	<link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond:300,300i,400,400i,500,500i,600,600i&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.typekit.net/czx8tmw.css">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header>
	<?php get_template_part( 'template-parts/mini-cart' ); ?>

	<div class="large flex">
		<a class="logo" href="<?php bloginfo('url'); ?>"><?php include(get_template_directory() . '/src/img/logo.svg'); ?></a>

		<div class="menu flex">
			<div class="hamburger"></div>
			<div class="cart-link"></div>
		</div>
	</div>
</header>

<?php include('inc-edit.php'); ?>

<div class="page">

	<!-- <div class="crumbs">
		<div class="large">
			<?php the_breadcrumb(); ?>
		</div>
	</div> -->
