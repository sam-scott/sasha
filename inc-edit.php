<?php

if ( is_user_logged_in() ) :

	global $current_user;
	get_currentuserinfo();

	if (is_category()){
			edit_term_link('<p class="edit_link">','</p>', $category, true );
	} elseif (is_tax()){
			edit_term_link( '<p class="edit_link">','</p>', $category, true );
	} elseif (is_tag()){
		edit_tag_link( '<p class="edit_link">','</p>', $category, true  );
	} else {
		edit_post_link( '<p class="edit_link">', '</p>' );
	}

endif;

?>
