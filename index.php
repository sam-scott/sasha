<?php

// Template Name: Projects Page

get_header(); ?>

<?php include('inc-edit.php');?>

<!-- banner -->

<?php get_template_part( 'template-parts/banner' ); ?>

<!-- categories -->

<?php get_template_part( 'template-parts/category-nav' ); ?>

<!-- projects -->

<section class="thick">
	<div class="large projects flex">
	  <?php while ( have_posts() ) : the_post();
	    get_template_part('template-parts/project-item');
	  endwhile; ?>
	</div>

	<?php numeric_posts_nav(); ?>

</section>



<!-- call to action -->

<?php get_template_part( 'template-parts/action' ); ?>


<?php get_footer(); ?>
