<?php get_header(); ?>

<?php include('inc-edit.php');?>

<section class="thick light">
	<div class="medium">
		<h2>Shop All</h2>

		<div class="xthin"></div>

		<?php global $product;
		$args = array( 'post_type' => 'product', 'posts_per_page' => 12, );
		$products = get_posts( $args ); ?>

		<div class="products flex">
			<?php foreach( $products as $item ): setup_postdata( $product ); ?>
				<?php include(locate_template('template-parts/product-item.php')); ?>
			<?php endforeach; wp_reset_postdata(); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
