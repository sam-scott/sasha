<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<!-- banner -->

	<?php get_template_part( 'template-parts/banner' ); ?>

	<!-- content -->

	<section class="thick">
		<div class="medium content">
			<?php the_content(); ?>
		</div>
	</section>
	
<?php endwhile; ?>

<?php  get_footer(); ?>
