<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
<?php endwhile; ?>

<?php get_template_part('template-parts/front-slider'); ?>

<section class="thin light">
	<div class="medium">
		<div class="tacenter">
			<h3>Explore <?php the_title(); ?></h3>
		</div>

		<div class="xthin"></div>

		<?php global $product;
		$args = array( 'post_type' => 'product', 'posts_per_page' => 12, );
		$products = get_posts( $args ); ?>

		<div class="products flex">
			<?php foreach( $products as $item ): setup_postdata( $product ); ?>
				<?php include(locate_template('template-parts/product-item.php')); ?>
			<?php endforeach; wp_reset_postdata(); ?>
		</div>
	</div>
</section>

<?php get_template_part('template-parts/banner'); ?>

<?php get_footer(); ?>
