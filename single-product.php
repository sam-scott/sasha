<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part('template-parts/product-slider'); ?>

	<section class="thin">
		<div class="small">
			<?php if( have_rows('tabs') ): ?>
			  <div class="accordion">
			    <?php while( have_rows('tabs') ): the_row();?>
			      <div class="item">
			        <div class="title">
			          <h6><?php the_sub_field('title'); ?></h6>
			        </div>

			        <div class="expand">
			          <div class="content">
			            <?php the_sub_field('content'); ?>
			          </div>
			        </div>
			      </div>
			    <?php endwhile; ?>
			  </div>
			<?php endif; ?>
		</div>
	</section>

	<?php get_template_part('template-parts/product-list'); ?>

<?php endwhile; ?>

<?php get_footer(); ?>
