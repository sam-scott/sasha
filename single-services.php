<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<div class="single-service">

		<!-- banner -->

		<?php include(locate_template('template-parts/banner.php')); ?>

		<!-- content -->

		<section class="thick dark">
			<div class="large flex content">
				<div class="item-45">
					<?php the_field('content'); ?>

					<?php if (get_field('button')): $button = get_field('button'); ?>
						<a href="<?php echo $button['url'] ?>"><?php echo $button['title'] ?></a>
					<?php endif; ?>
				</div>

				<div class="item-45 image">
					<?php $image = get_field('image'); ?>
					<div class="background" style="background-image: url(<?php echo $image['sizes']['800w']; ?>)"></div>
				</div>
			</div>
		</section>

		<!-- gallery -->

		<section class="thick">
			<div class="large">
				<?php $title = get_field('gallery_title'); $subtitle = get_field('gallery_subtitle');
				include(locate_template('template-parts/title.php')); ?>

				<?php include(locate_template('template-parts/gallery.php')); ?>
			</div>
		</section>

		<!-- accordion -->

		<section class="thick light">
			<?php $title = get_field('accordion_title'); $subtitle = get_field('accordion_subtitle');
			include(locate_template('template-parts/title.php')); ?>

			<?php include(locate_template('template-parts/accordion.php')); ?>

			<?php if (get_field('download')): ?>
				<div class="xthin"></div>

				<div class="tacenter">
					<?php $file = get_field('download'); ?>
					<a class="button download" href="<?php echo $file['url']; ?>" target="_blank" rel="noreferer" rel="noreferrer">Download Brochure</a>
				</div>
			<?php endif; ?>
		</section>

		<!-- projects -->

		<section class="thick light">
			<?php $title = get_field('projects_title'); $subtitle = get_field('projects_subtitle');
			include(locate_template('template-parts/title.php')); ?>

			<?php $num = 3; $term = get_term(get_field('projects_category'), 'category'); $cat = $term->term_id; ?>
			<?php include(locate_template('template-parts/projects.php')); ?>

			<div class="xthin"></div>

			<div class="tacenter">
				<a class="button" href="<?php the_permalink(11); ?>">View all</a>
			</div>
		</section>

	</div>

<?php endwhile; ?>

<?php  get_footer(); ?>
