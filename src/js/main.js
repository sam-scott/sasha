jQuery(document).ready(function($) {
  // hamburger toggle
  $('.hamburger').on('click', function(e) {
    $('header').toggleClass('active');
    e.preventDefault();
  });

  // submenu toggle
  $('.nav .menu-item-has-children').on('click', function() {
    $(this).toggleClass('open');
  });

  // scrolling header
  // $(window).scroll(function() {
  //   var scroll = $(window).scrollTop();
  //   if (scroll >= 200) {
  //     $("header").addClass("scrolled");
  //   } else {
  //     $("header").removeClass("scrolled");
  //   }
  // });

  $('header').on('click', '.cart-link', function() {
    $('.mini-cart').toggleClass('active');
  });

  // $('.mini-cart').on('click', '.close', function() {
  //   $('.mini-cart').removeClass('active');
  // });

  // $('.mini-cart .close-cart').on('click', function() {
  //   $('.mini-cart')
  //     .toggleClass('active')
  //     .fadeOut(300);
  // });

  // accordion
  $('.accordion .item').on('click', '.title', function(e) {
    e.preventDefault();
    $(this).toggleClass('open');
  });
  // accordion
  $('.product-tabs').on('click', '.tab-nav .item', function(e) {
    $('.tab-display')
      .find('.item')
      .toggleClass('active');
  });

  // front slider
  $('.front-slider .main-carousel').flickity({
    cellAlign: 'left',
    wrapAround: false,
    draggable: false,
    pageDots: false,
    prevNextButtons: true
  });

  // product slider
  $('.product-slider .main-carousel').flickity({
    cellAlign: 'left',
    wrapAround: false,
    draggable: true,
    pageDots: true,
    prevNextButtons: false
  });

  // // slider
  // $('.main-carousel.product-gallery').flickity({
  //   cellAlign: 'left',
  //   wrapAround: false,
  //   draggable: true,
  //   pageDots: true,
  //   prevNextButtons: false
  // });
  //
  // // testimonials
  // $('.main-carousel.testimonials').flickity({
  //   cellAlign: 'center',
  //   wrapAround: true,
  //   pageDots: true,
  //   prevNextButtons: false,
  //   adaptiveHeight: true
  // });
  //
  // // gallery
  // $('.main-carousel.gallery').flickity({
  //   cellAlign: 'center',
  //   wrapAround: true,
  //   pageDots: false,
  //   prevNextButtons: true
  // });
});
