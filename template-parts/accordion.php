<?php if( have_rows('accordion') ): ?>
  <div class="medium accordion">
    <?php while( have_rows('accordion') ): the_row();?>
      <div class="item">
        <div class="title">
          <h3><?php the_sub_field('title'); ?></h3>
        </div>

        <div class="expand">
          <div class="content">
            <?php the_sub_field('content'); ?>
          </div>
        </div>
      </div>
    <?php endwhile; ?>
  </div>
<?php endif; ?>
