<section class="thick">
  <div class="thick"></div>

  <div class="large tdark flex">
    <h2><?php the_field('cta_title', 'options'); ?></h2>

    <div class="item-65">
      <p><strong><?php the_field('cta_desc', 'options'); ?></strong></p>
    </div>

    <div class="item-30 taright">
      <?php $button = get_field('cta_button', 'options'); ?>
      <a class="button" href="<?php echo $button['url'] ?>"><?php echo $button['title'] ?></a>
    </div>
  </div>

  <?php $image = get_field('cta_image', 'options'); ?>
  <div class="background dark" style="background-image: url(<?php echo $image['sizes']['1200w']; ?>);"></div>
</section>
