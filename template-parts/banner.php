<section class="banner thick">
  <div class="small tdark tacenter">
    <h2><?php the_field('banner_title'); ?></h2>

    <?php if (get_field('banner_description')): ?>
      <p><?php the_field('banner_description'); ?></p>
    <?php endif; ?>

    <?php if (get_field('banner_button')): ?>
      <a class="button white" href="<?php the_field('banner_button'); ?>">Tell me more</a>
    <?php endif; ?>
  </div>

  <?php $image = get_field('banner_image'); ?>
  <div class="background dark" style="background-image: url(<?php echo $image['sizes']['1200w']; ?>);"></div>
</section>
