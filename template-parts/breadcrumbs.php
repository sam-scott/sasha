<?php global $post;
if ($post->post_parent) :
  $parent = get_the_title( $post->post_parent );
  $current = get_the_title( $post ); ?>
  <div class="large crumbs"> <a href="<?php the_permalink($post->post_parent); ?>"><?php echo $parent ?></a> > <strong><?php echo $current ?></strong></div>
<?php endif; ?>
