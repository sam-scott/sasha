<section class="collections flex third">
  <?php $items = get_field('collections'); foreach( $items as $item ): ?>
    <a href="<?php echo get_the_permalink($item->ID); ?>" class="item relative flex tdark">
      <h3><strong><?php echo get_the_title($item->ID); ?></strong></h3>

      <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ), '800w' ); ?>
      <div class="background dark" style="background-image: url(<?php echo $image[0]; ?>);"></div>
    </a>
  <?php endforeach;  ?>
</section>
