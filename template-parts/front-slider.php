<section class="front-slider">
  <?php if( have_rows('slider') ): ?>
    <div class="main-carousel">
      <?php while( have_rows('slider') ): the_row();?>
        <div class="carousel-cell flex">
          <div class="item-50 relative">
            <?php $image = get_sub_field('image'); ?>
            <div class="background" style="background-image: url(<?php echo $image['sizes']['1200w']; ?>);"></div>
          </div>

          <div class="item-50 flex light">
            <div class="content">
              <h1><?php the_sub_field('title'); ?></h1>
              <p><?php the_sub_field('description'); ?></p>

              <a class="button" href="<?php the_sub_field('button'); ?>">Shop Now</a>
            </div>
          </div>
        </div>
      <?php endwhile; ?>
    </div>
  <?php endif; ?>
</section>
