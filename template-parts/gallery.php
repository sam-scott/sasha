<?php if( have_rows('gallery') ): ?>
  <div class="main-carousel gallery">
    <?php while( have_rows('gallery') ): the_row();?>
      <div class="carousel-cell">
        <?php $image = get_sub_field('image'); ?>
        <div class="background" style="background-image: url(<?php echo $image['sizes']['1200w']; ?>);"></div>
      </div>
    <?php endwhile; ?>
  </div>
<?php endif; ?>
