<section class="opener flex">
  <div class="small tdark tacenter">
    <h1><?php the_field('opener_title'); ?></h1>

    <?php $button = get_field('opener_button'); ?>
    <a class="button" href="<?php echo $button['url']; ?>"><?php echo $button['title']; ?></a>
  </div>

  <?php $image = get_field('opener_image'); ?>
  <div class="background dark" style="background-image: url(<?php echo $image['sizes']['1800w'] ?>);"></div>
</section>
