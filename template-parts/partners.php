<section class="thin logos">
  <?php if (get_field('logos_title')): ?>
    <div class="tacenter">
      <h3><?php the_field('logos_title') ?></h3>
    </div>

    <div class="xthin"></div>
  <?php endif; ?>

  <?php if( have_rows('logos') ): ?>
    <div class="medium logos flex">
      <?php while( have_rows('logos') ): the_row();?>
        <div class="item">
          <?php $image = get_sub_field('image') ?>
          <img src="<?php echo $image['sizes']['600w'] ?>" alt="<?php  echo $image['alt'] ?>">
        </div>
      <?php endwhile; ?>
    </div>
  <?php endif; ?>
</section>
