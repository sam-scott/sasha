<a href="<?php echo get_the_permalink($item->ID); ?>" class="item-20 tacenter">
  <div class="image relative">
    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ), '800w' ); ?>
    <div class="background" style="background-image: url(<?php echo $image[0]; ?>);"></div>
  </div>

  <div class="meta">
    <h4><?php echo get_the_title($item->ID); ?></h4>
    <h6 class="price">$<?php echo get_post_meta( $item->ID, '_price', true ); ?></h6>
  </div>
</a>
