<section class="light thin">
  <div class="medium">
    <h2><?php the_field('products_title'); ?></h2>

    <div class="xthin"></div>

    <div class="products flex">
      <?php $items = get_field('products'); foreach( $items as $item ): ?>
        <?php include(locate_template('template-parts/product-item.php')); ?>
      <?php endforeach;  ?>
    </div>
  </div>
</section>
