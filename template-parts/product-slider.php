<section class="product-slider flex">

  <?php global $product; ?>

  <div class="item-50 light flex">
    <div class="content">
      <a class="back" href="<?php the_permalink(30); ?>"><h6>Back to shop</h6></a>

      <h1><?php the_title(); ?></h1>
      <p><?php the_field('description'); ?></p>

      <h5 class="cost">$<?php echo $product->get_price(); ?> AUD</h5>
      <?php do_action( 'woocommerce_single_product_summary' ); ?>

      <div class="clear"></div>
    </div>
  </div>

  <div class="item-50 relative">
    <div class="main-carousel">
      <?php $attachment_ids = $product->get_gallery_attachment_ids();
      foreach( $attachment_ids as $attachment_id ) : ?>
        <div class="carousel-cell relative">
          <?php $image = wp_get_attachment_image_src( $attachment_id, '1200w' )[0]; ?>
          <div class="background" style="background-image: url(<?php echo $image; ?>);"></div>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</section>
