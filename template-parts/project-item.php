<a class="item-30" href="<?php the_permalink(); ?>">
  <div class="image flex">
    <div class="button white">View project</div>

    <?php $image = get_the_post_thumbnail_url(get_the_ID($post->ID), '800w'); ?>
    <div class="background" style="background-image: url(<?php echo $image ?>);"></div>
  </div>

  <div class="meta">
    <?php $cats = get_the_category(); foreach ( $cats as $cat ): ?>
      <span><?php echo $cat->name; ?></span>
    <?php endforeach; ?>
  </div>

  <h4><?php the_title(); ?></h4>
</a>
