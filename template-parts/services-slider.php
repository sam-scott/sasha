<?php global $post;
$args = array( 'post_type' => 'services');
$posts = get_posts( $args ); ?>

<div class="main-carousel services">
  <?php foreach ( $posts as $post ) : setup_postdata( $post ); ?>
    <a class="carousel-cell" href="<?php the_permalink(); ?>">
      <div class="image">
        <?php $image = get_the_post_thumbnail_url(get_the_ID($post->ID), '800w'); ?>
        <div class="background" style="background-image: url(<?php echo $image ?>);"></div>
      </div>
      <h3><?php the_title(); ?></h3>
    </a>
  <?php endforeach; wp_reset_postdata();?>
</div>
