<?php global $post;
$args = array( 'post_type' => 'services', 'posts_per_page' => 6 );
$posts = get_posts( $args ); ?>

<section class="services flex tdark">
  <?php foreach ( $posts as $post ) : setup_postdata( $post ); ?>
    <div class="item-50 image flex">
      <div class="tacenter">
        <h5><?php the_title(); ?></h5>
        <a class="button white" href="<?php the_permalink(); ?>">View More</a>
      </div>

      <?php $image = get_the_post_thumbnail_url(get_the_ID($post->ID), '800w'); ?>
      <div class="background dark" style="background-image: url(<?php echo $image ?>);"></div>
    </div>
  <?php endforeach; wp_reset_postdata();?>
</section>
