<div class="share flex">
   <a href="https://www.facebook.com/sharer/sharer.php?u=&t=" target="_blank" rel="noreferer" title="Share on Facebook" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '&t=' + encodeURIComponent(document.URL)); return false;">
     <i class="fab fa-facebook-f" aria-hidden="true"></i><span class="sr-only">Share on Facebook</span>
   </a>

   <a href="https://twitter.com/intent/tweet?source=&text=:%20" target="_blank" rel="noreferer" title="Tweet" onclick="window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(document.title) + ':%20' + encodeURIComponent(document.URL)); return false;">
     <i class="fab fa-twitter" aria-hidden="true"></i><span class="sr-only">Tweet</span>
   </a>

   <a href="https://www.pinterest.com/pinterest/" target="_blank" rel="noreferer" title="Pinterest" onclick="window.open('https://pinterest.com/pin/create/link/?url=' + encodeURIComponent(document.URL) + '&description=' + encodeURIComponent(document.title) + '&media=<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)) ?>');">
     <i class="fab fa-pinterest-p" aria-hidden="true"></i><span class="sr-only">Pinterest</span>
   </a>

   <a href="https://plus.google.com/share?url=" target="_blank" rel="noreferer" title="Google Plus" onclick="window.open('https://plus.google.com/share?url=' + encodeURIComponent(document.URL); return false;">
     <i class="fab fa-google-plus-g" aria-hidden="true"></i><span class="sr-only">Google Plus</span>
   </a>
</div>
