<div class="social">
  <?php if (get_field('facebook', 'options')): ?>
    <a href="<?php the_field('facebook', 'options'); ?>"><i class="fab fa-facebook-f"></i></a>
  <?php endif; ?>
  <?php if (get_field('twitter', 'options')): ?>
    <a href="<?php the_field('twitter', 'options');?>"><i class="fab fa-twitter"></i></a>
  <?php endif; ?>
  <?php if (get_field('linkedin', 'options')): ?>
    <a href="<?php the_field('linkedin', 'options'); ?>"><i class="fab fa-linkedin-in"></i></a>
  <?php endif; ?>
  <?php if (get_field('instagram', 'options')): ?>
    <a href="<?php the_field('instagram', 'options'); ?>"><i class="fab fa-instagram"></i></a>
  <?php endif; ?>
  <?php if (get_field('pinterest', 'options')): ?>
    <a href="<?php the_field('pinterest', 'options');?>"><i class="fab fa-pinterest-p"></i></a>
  <?php endif; ?>
</div>
