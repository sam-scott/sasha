<?php if( have_rows('team') ): ?>
  <section class="thick">
    <?php if (get_field('team_title')): ?>
      <div class="tacenter">
        <h6><?php the_field('team_subtitle'); ?></h6>
        <h2><?php the_field('team_title'); ?></h2>
      </div>

      <div class="xthin"></div>
    <?php endif; ?>

    <div class="large team flex">
      <?php while( have_rows('team') ): the_row();?>
        <div class="item-20">
          <div class="image">
            <?php $image = get_sub_field('image'); ?>
            <div class="background" style="background-image: url(<?php echo $image['sizes']['800w']; ?>)"></div>
          </div>

          <div class="meta"><span><?php the_sub_field('position'); ?></span></div>
          <h4><?php the_sub_field('name'); ?></h4>
          <p class="sm"><?php the_sub_field('desc'); ?></p>
        </div>
      <?php endwhile; ?>
    </div>

    <?php if (get_field('team_button')): ?>
      <div class="xthin"></div>

      <div class="tacenter">
        <?php $button = get_field('team_button'); ?>
        <a class="button" href="<?php echo $button['url']; ?>"><?php echo $button['title']; ?></a>
      </div>
    <?php endif; ?>
  </section>
<?php endif; ?>
