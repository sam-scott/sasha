<?php if ( have_rows('testimonials') ) : ?>
  <div class="main-carousel testimonials">
    <?php while ( have_rows('testimonials') ) : the_row(); ?>
      <div class="carousel-cell tacenter">
        <p><?php the_sub_field('quote'); ?></p>
        <h6>- <?php the_sub_field('name'); ?></h6>
      </div>
    <?php endwhile; ?>
  </div>
<?php endif; ?>
