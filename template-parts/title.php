<?php if ($title): ?>
  <div class="tacenter">
    <h6><?php echo $subtitle ?></h6>
    <h2><?php echo $title ?></h2>
  </div>

  <div class="xthin"></div>
<?php endif; ?>
