<?php

// Template Name: About Page

get_header(); ?>

<!-- banner -->

<?php get_template_part( 'template-parts/banner' ); ?>

<!-- content -->

<section class="thick dark">
	<div class="large tdark content">
		<?php the_field('about_content'); ?>
	</div>
</section>

<!-- services -->

<?php get_template_part( 'template-parts/services' ); ?>

<!-- team -->

<?php get_template_part( 'template-parts/team' ); ?>

<!-- call to action -->

<?php get_template_part( 'template-parts/action' ); ?>

<?php  get_footer(); ?>
