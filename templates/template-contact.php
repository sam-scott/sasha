<?php

// Template Name: Contact Page

get_header(); ?>

<!-- banner -->

<?php get_template_part( 'template-parts/banner' ); ?>

<!-- form -->

<section class="thick dark">
	<div class="large flex">
		<div class="item-45">
			<h3><?php the_field('form_title') ?></h3>
			<p><?php the_field('form_description') ?></p>

			<?php if ( get_field('phone', 'options') ) : ?>
				<h6>Call us</h6>
				<p><a href="tel:<?php the_field('phone', 'options'); ?>"><?php the_field('phone', 'options'); ?></a></p>
			<?php endif; ?>

			<?php if ( get_field('email', 'options') ) : ?>
				<h6>Email us</h6>
				<p><a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a></p>
			<?php endif; ?>

			<?php if ( get_field('address', 'options') ) : ?>
				<h6>Find us here</h6>
				<p><?php the_field('address', 'options'); ?></p>
			<?php endif; ?>
		</div>

		<div class="item-45">
			<?php $shortcode = get_field('form_shortcode') ?>
			<?php echo do_shortcode($shortcode) ?>
		</div>
	</div>
</section>

<section class="map">
	<div class="dark absolute tdark"><a href="<?php the_field('map_link'); ?>" target="_blank" rel="noreferer"><h6>View on Google Maps</h6></a></div>

	<?php $image = get_field('map_image'); ?>
	<div class="background" style="background-image: url(<?php echo $image['url'] ?>)"></div>
</section>

<?php get_footer(); ?>
