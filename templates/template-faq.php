<?php

// Template Name: FAQ Page

get_header(); ?>

<!-- banner -->

<?php get_template_part( 'template-parts/banner' ); ?>

<!-- accordion -->

<section class="thick">
	<?php get_template_part( 'template-parts/accordion' ); ?>
</section>

<?php get_footer(); ?>
