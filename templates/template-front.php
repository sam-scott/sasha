<?php

// Template Name: Front Page

get_header(); ?>

<!-- slider -->

<?php get_template_part('template-parts/front-slider'); ?>

<!-- about -->

<section class="thick">
	<div class="medium flex">
		<div class="item-45">
			<h2><?php the_field('about_title'); ?></h2>
			<p><?php the_field('about_description'); ?></p>

			<a class="button" href="<?php the_field('about_button'); ?>">Our Story</a>
		</div>

		<div class="item-45 feature">
			<?php $image = get_field('about_image'); ?>
			<div class="background" style="background-image: url(<?php echo $image['sizes']['800w']; ?>);"></div>
		</div>
	</div>
</section>

<!-- collections -->

<?php get_template_part('template-parts/collections'); ?>

<!-- products -->

<?php get_template_part('template-parts/product-list'); ?>

<!-- banner -->

<?php get_template_part('template-parts/banner'); ?>

<?php  get_footer(); ?>
